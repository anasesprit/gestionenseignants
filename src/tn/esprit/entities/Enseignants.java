package tn.esprit.entities;
import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Enseignants implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id_enseignants;
	private Calendrier dispo;
	private Status status;
	private List<Enseigner> enseignant;
	@Id
	@GeneratedValue
	public int getId_enseignants() {
		return id_enseignants;
	}
	public void setId_enseignants(int id_enseignants) {
		this.id_enseignants = id_enseignants;
	}
	public Calendrier getDispo() {
		return dispo;
	}
	public void setDispo(Calendrier dispo) {
		this.dispo = dispo;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	@OneToMany(mappedBy="enseignant")
	public List<Enseigner> getEnseignant() {
		return enseignant;
	}
	public void setEnseignant(List<Enseigner> enseignant) {
		this.enseignant = enseignant;
	}
	

}
