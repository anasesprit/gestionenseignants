package tn.esprit.entities;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
@Entity
public class Enseigner {
	@EmbeddedId
     private EnseignerPK idEnseiger;
     private Enseignants enseignants;
     private Matiere matiere;
     private Classe classe;
	public EnseignerPK getIdEnseiger() {
		return idEnseiger;
	}
	public void setIdEnseiger(EnseignerPK idEnseiger) {
		this.idEnseiger = idEnseiger;
	}
	@ManyToOne
	public Enseignants getEnseignants() {
		return enseignants;
	}
	public void setEnseignants(Enseignants enseignants) {
		this.enseignants = enseignants;
	}
	@ManyToOne
	public Matiere getMatiere() {
		return matiere;
	}
	public void setMatiere(Matiere matiere) {
		this.matiere = matiere;
	}
	@ManyToOne
	public Classe getClasse() {
		return classe;
	}
	public void setClasse(Classe classe) {
		this.classe = classe;
	}
     
     
}
