package tn.esprit.entities;

import java.util.Date;

import javax.persistence.Entity;
@Entity
public class Calendrier {
     private Date date;
     private Disponible disponible;
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
}
