package tn.esprit.entities;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Matiere {
     private int idmatiere;
     private String nom;
     private List<Enseigner> enseigner;
    
	public Matiere(int idmatiere, String nom, List<Enseigner> enseigner) {
		super();
		this.idmatiere = idmatiere;
		this.nom = nom;
		this.enseigner = enseigner;
	}
	@Id
	@GeneratedValue
	public int getIdmatiere() {
		return idmatiere;
	}
	public void setIdmatiere(int idmatiere) {
		this.idmatiere = idmatiere;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	@OneToMany(mappedBy="matiere")
	public List<Enseigner> getEnseigner() {
		return enseigner;
	}
	public void setEnseigner(List<Enseigner> enseigner) {
		this.enseigner = enseigner;
	}
     
}
