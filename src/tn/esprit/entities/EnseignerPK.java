package tn.esprit.entities;

import javax.persistence.Embeddable;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Embeddable
public class EnseignerPK {
	   private int id_Enseignant;
	   private int matrice_Id;
	   private int id_classe;
	   @Id
	   @GeneratedValue
	public int getId_Enseignant() {
		return id_Enseignant;
	}
	public void setId_Enseignant(int id_Enseignant) {
		this.id_Enseignant = id_Enseignant;
	}
	public int getMatrice_Id() {
		return matrice_Id;
	}
	public void setMatrice_Id(int matrice_Id) {
		this.matrice_Id = matrice_Id;
	}
	public int getId_classe() {
		return id_classe;
	}
	public void setId_classe(int id_classe) {
		this.id_classe = id_classe;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id_Enseignant;
		result = prime * result + id_classe;
		result = prime * result + matrice_Id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EnseignerPK other = (EnseignerPK) obj;
		if (id_Enseignant != other.id_Enseignant)
			return false;
		if (id_classe != other.id_classe)
			return false;
		if (matrice_Id != other.matrice_Id)
			return false;
		return true;
	}

	   
}
