package tn.esprit.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class Classe {
        private int id_classe;
        private int nom;
        private List<Enseigner> enseigner;
        @Id
        @GeneratedValue
 		public int getId_classe() {
			return id_classe;
		}
		public void setId_classe(int id_classe) {
			this.id_classe = id_classe;
		}
		public int getNom() {
			return nom;
		}
		public void setNom(int nom) {
			this.nom = nom;
		}
		@OneToMany(mappedBy="classe")
		public List<Enseigner> getEnseigner() {
			return enseigner;
		}
		public void setEnseigner(List<Enseigner> enseigner) {
			this.enseigner = enseigner;
		}
        
}
